package be.timsys.gaming.acc.director.game.model.response;

import be.timsys.gaming.acc.director.game.model.AccMessage;
import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
public class TrackData extends AccMessage {

    private int connectionId;
    private String trackName;
    private int trackId;
    private int trackMeters;
    private byte cameraSetCount;
    private List<CameraSet> cameraSets;

    public TrackData(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
    }

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        this.connectionId = lil.readInt();
        this.trackName = readString(lil);
        this.trackId = lil.readInt();
        this.trackMeters = lil.readInt();
        this.cameraSetCount = lil.readByte();

        readCameraConfig(this.cameraSetCount, lil);
    }

    private void readCameraConfig(int cameraSetCount, LittleEndianDataInputStream lil) throws IOException {
        this.cameraSets = new ArrayList<>();
        for (int i = 0; i < cameraSetCount; i++) {
            cameraSets.add(readCameraSet(lil));
        }
    }

    private CameraSet readCameraSet(LittleEndianDataInputStream lil) throws IOException {
        var set = new CameraSet(readString(lil));
        set.setCameraCount(lil.readByte());
        for (int i = 0; i < set.getCameraCount(); i++) {
            set.add(new Camera(readString(lil)));
        }

        return set;
    }
}
