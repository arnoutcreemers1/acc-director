package be.timsys.gaming.acc.director;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccDirectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccDirectorApplication.class, args);
	}

}
