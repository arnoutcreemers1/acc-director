package be.timsys.gaming.acc.director.game.model.response;

import be.timsys.gaming.acc.director.game.model.AccMessage;
import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;

@Getter
@ToString
public class RealTimeCarUpdate extends AccMessage {

    private short carIndex;
    private short driverIndex;
    private byte driverCount;
    private int gear;
    private float worldPosX;
    private float worldPosY;
    private float yaw;
    private byte carLocation;
    private short kmh;
    private short sessionPosition;
    private short sessionCupPosition;
    private short sessionTrackPosition;
    private float splinePosition;
    private short laps;
    private int deltaBestSessionLap;
    private Lap bestSessionLap;
    private Lap lastLap;
    private Lap currentLap;

    public RealTimeCarUpdate(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
    }

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        this.carIndex = lil.readShort();
        this.driverIndex = lil.readShort();
        this.driverCount = lil.readByte();
        this.gear = lil.readByte() - 2;
        this.worldPosX = lil.readFloat();
        this.worldPosY = lil.readFloat();
        this.yaw = lil.readFloat();
        this.carLocation = lil.readByte();
        this.kmh = lil.readShort();
        this.sessionPosition = lil.readShort();
        this.sessionCupPosition = lil.readShort();
        this.sessionTrackPosition = lil.readShort();
        this.splinePosition = lil.readFloat();
        this.laps = lil.readShort();
        this.deltaBestSessionLap = lil.readInt();
        this.bestSessionLap = new Lap(lil);
        this.lastLap = new Lap(lil);
        this.currentLap = new Lap(lil);
    }
}
