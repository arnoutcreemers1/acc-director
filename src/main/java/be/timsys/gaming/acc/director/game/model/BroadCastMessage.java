package be.timsys.gaming.acc.director.game.model;

import com.google.common.io.LittleEndianDataInputStream;
import lombok.Data;

import java.io.IOException;

@Data
public class BroadCastMessage extends AccMessage {
    private byte type;
    private String message;
    private int timeMs;
    private int carId;

    public BroadCastMessage(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
    }

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        type = lil.readByte();
        message = readString(lil);
        timeMs = lil.readInt();
        carId = lil.readInt();
    }
}
