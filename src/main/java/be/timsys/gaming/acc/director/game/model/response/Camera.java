package be.timsys.gaming.acc.director.game.model.response;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
class Camera {

    private final String name;
}
