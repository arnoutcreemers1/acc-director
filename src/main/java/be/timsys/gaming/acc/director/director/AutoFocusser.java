package be.timsys.gaming.acc.director.director;

import be.timsys.gaming.acc.director.director.model.Car;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
class AutoFocusser {

    private short lastListIndex = -1;

    Car getNextFocusCar(List<Car> cars) {
        short nextListIndex = ++lastListIndex;

        if (nextListIndex >= cars.size()) {
            nextListIndex = 0;
        }
        lastListIndex = nextListIndex;
        return cars.get(nextListIndex);
    }

    void clear() {
        this.lastListIndex = -1;
    }
}
