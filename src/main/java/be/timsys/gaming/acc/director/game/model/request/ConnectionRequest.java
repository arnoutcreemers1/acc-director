package be.timsys.gaming.acc.director.game.model.request;

import be.timsys.gaming.acc.director.game.model.StructWriter;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;

@Getter
@ToString
public class ConnectionRequest {

    private final int version;
    private final String name;
    private final String connectionPwd;
    private final String commandPwd;
    private final int refreshInterval;

    public ConnectionRequest(int version, String name, String connectionPwd, String commandPwd, int refreshInterval) {
        this.version = version;
        this.name = name;
        this.connectionPwd = connectionPwd;
        this.commandPwd = commandPwd;
        this.refreshInterval = refreshInterval;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(60);
        structWriter.writeByte((byte) 1);
        structWriter.writeByte((byte) version);
        structWriter.writeString(name);
        structWriter.writeString(connectionPwd);
        structWriter.writeInt(refreshInterval);
        structWriter.writeString(commandPwd);
        return structWriter.toByteArray();
    }
}
