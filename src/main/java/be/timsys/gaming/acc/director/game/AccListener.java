package be.timsys.gaming.acc.director.game;

import be.timsys.gaming.acc.director.game.model.AccMessage;
import be.timsys.gaming.acc.director.game.model.response.Connection;
import be.timsys.gaming.acc.director.game.model.response.RealTimeCarUpdate;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;

@Service
@RequiredArgsConstructor
public class AccListener {

    private AccClient accClient;
    private SocketReader socketReader;

    private final AccMessageFactory accMessageFactory;
    private final ApplicationEventPublisher publisher;
    private Thread thread;

    void startListening(InetAddress ip, int port) throws Exception {
        accClient = new AccClient();
        accClient.init(ip, port);

        socketReader = new SocketReader();

        thread = new Thread(socketReader, "SocketReader");
        thread.start();

        accClient.requestConnection();
    }

    void stop(Connection connection) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.shutdown(connection);
            accClient = null;
            thread.stop();
        }
    }

    void focus(Connection connection, short carIndex) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.requestFocus(connection, carIndex);
        }
    }

    void hudPage(Connection connection, String hudPage) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.requestHud(connection, hudPage);
        }
    }

    void requestEntryList(Connection connection) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.requestEntryList(connection);
        }
    }

    private class SocketReader implements Runnable {
        @Override
        public void run() {
            if (accClient != null) {
                try {
                    byte[] read = accClient.read();
                    while (read != null) {
                        processMessage(read);
                        read = accClient.read();
                    }
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        }

        private void processMessage(byte[] read) throws IOException {
            AccMessage accMessage = accMessageFactory.create(read);

            if (accMessage instanceof Connection) {
                accClient.requestEntryList((Connection) accMessage);
                accClient.requestTrackData((Connection) accMessage);
            }

            if (accMessage != null) {
                if (!(accMessage instanceof RealTimeCarUpdate)) {
                    System.out.println(accMessage);
                }
                publisher.publishEvent(accMessage);
            }
        }
    }
}
