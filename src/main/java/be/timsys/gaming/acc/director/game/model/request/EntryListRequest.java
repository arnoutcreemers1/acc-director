package be.timsys.gaming.acc.director.game.model.request;

import be.timsys.gaming.acc.director.game.model.StructWriter;

import java.io.IOException;

public class EntryListRequest {
    private int connectionId;

    public EntryListRequest(int connectionId) {
        this.connectionId = connectionId;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(60);
        structWriter.writeByte((byte) 10);
        structWriter.writeInt(connectionId);

        return structWriter.toByteArray();
    }
}
