package be.timsys.gaming.acc.director.game.model.request;

import be.timsys.gaming.acc.director.game.model.StructWriter;

import java.io.IOException;

public class TrackDataRequest {
    private int connectionId;

    public TrackDataRequest(int connectionId) {
        this.connectionId = connectionId;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(60);
        structWriter.writeByte((byte) 11);
        structWriter.writeInt(connectionId);

        return structWriter.toByteArray();
    }
}
