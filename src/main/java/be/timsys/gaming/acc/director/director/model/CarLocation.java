package be.timsys.gaming.acc.director.director.model;

import java.util.stream.Stream;

public enum CarLocation {
    NONE((short) 0), TRACK((short) 1), PITLANE((short) 2), PITENTRY((short) 3), PITEXIT((short) 4);


    private short id;

    CarLocation(short id) {
        this.id = id;
    }

    public static CarLocation fromId(short id) {
        return Stream.of(values()).filter(cl -> cl.id == id).findFirst().orElse(null);
    }

}
